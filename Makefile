# File: Makefile
# Author: Bruno Correia <brunogomescorreia@gmail.com>
# Created: Fri 23 Jul 2020 08:20:01 UTC-3
# Notes: Makefile for to create environment
# Execute "make" for help
###############################################################################
help:
	@echo 'Use:	"make create" or "make delete"';

service_account:
	@echo "Target name: $@"
	@if [ ! -f "$(GCP_SERVICE_ACCOUNT_FILE)" ]; then \
		gcloud iam service-accounts keys create $(GCP_SERVICE_ACCOUNT_FILE) \
  			--iam-account $(GCP_SERVICE_ACCOUNT_MAIL); \
	fi
	
create: service_account
	@echo "Target name: $@"
	@terraform init;
	@terraform plan -var gcloud_auth_file_path=$(GCP_SERVICE_ACCOUNT_FILE) -var project_id=$(PROJECT_ID) -var client_email=$(GCP_PROJECT_NUMBER)-compute@developer.gserviceaccount.com -var instance_name="hello-world" -var zone="southamerica-east1-b";
	@terraform apply -var gcloud_auth_file_path=$(GCP_SERVICE_ACCOUNT_FILE) -var project_id=$(PROJECT_ID) -var client_email=$(GCP_PROJECT_NUMBER)-compute@developer.gserviceaccount.com -var instance_name="hello-world" -var zone="southamerica-east1-b" -auto-approve;

delete: service_account
	@echo "Target name: $@"
	@terraform destroy -var gcloud_auth_file_path=$(GCP_SERVICE_ACCOUNT_FILE) -var project_id=$(PROJECT_ID) -var client_email=$(GCP_PROJECT_NUMBER)-compute@developer.gserviceaccount.com -var instance_name="hello-world" -var zone="southamerica-east1-b" -auto-approve;






