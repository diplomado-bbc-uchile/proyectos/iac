#!/usr/bin/env bash
export GCP_PROJECT_NUMBER=$(gcloud projects list | grep ${PROJECT_ID} | awk '{print $NF}');
export GCP_SERVICE_ACCOUNT_FILE=$(find ${HOME}/.config/gcloud/legacy_credentials/ -name adc.json);
export GCP_SERVICE_ACCOUNT_MAIL=${GCP_PROJECT_NUMBER}-compute@developer.gserviceaccount.com;
