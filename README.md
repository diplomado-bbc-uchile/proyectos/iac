# Instance with Container

This example illustrates how to deploy a container to a Google Compute Engine instance in GCP with advanced options.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| client\_email | Service account email address | string | n/a | yes |
| instance\_name | The desired name to assign to the deployed instance | string | n/a | yes |
| project\_id | The project ID to deploy resources into | string | n/a | yes |
| zone | The GCP zone to deploy instances into | string | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| container | The container metadata provided to the module |
| instance\_name | The deployed instance name |
| ipv4 | The public IP address of the deployed instance |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Running

To provision this example, run the following from within this directory:

- Define the environment variable `PROJECT_ID` (``export PROJECT_ID="project-id"``) and execute:

- `make create` to create instance
- `make delete` to delete instance